<!DOCTYPE HTML>
<html lang="en-ca">
 <head>
  <meta charset="UTF-8" />
  <title>Web "to do" list</title>
 </head>
 <body>
  <h1>Web "to do" list</h1>
  <?php
   # error_reporting(E_ALL);
   $mysqli = new mysqli(
    "localhost",
    "scott",
    "tiger",
    "webtodo"
   );
   if ($errno = $mysqli->connect_errno)
   {
    echo "<p>MySQL error #$errno</p>\n";
   }
   else
   {
    if (array_key_exists("url", $_POST))
    {
     $url = $_POST['url'];
     if (strlen(filter_var($url, FILTER_VALIDATE_URL)) > 0)
     {
      $caption = strip_tags($_POST['caption']);
      $statement = $mysqli -> prepare(<<<INS
       insert into webtodo(url, caption) values(?, ?)
INS
      );
      $statement->bind_param("ss", $url, $caption);
      $statement->execute();
      $statement->close();
     }
     else
     {
      echo "<h3>Not a real URL!</h3>\n";
     }
    }
    elseif (array_key_exists("id", $_GET))
    {
     $id = $_GET['id'];
     $statement = $mysqli -> prepare(<<<DEL
      delete from webtodo where id = ?
DEL
     );
     $statement->bind_param("i", $id);
     $statement->execute();
     $statement->close();
    }
    $result = $mysqli -> query(<<<SEL
     select id, url, caption from webtodo
SEL
    );
    if ($result)
    {
     while ($row = $result -> fetch_row())
     {
      echo "<p><a href=\"${row[1]}\">${row[2]}</a> (<a href=\".?id=${row[0]}\">delete</a>)</p>\n";
     }
    }
    echo "<h2><a href=\"create.php\">add new link</a></h2>\n";
   }
  ?>
 </body>
</html>

